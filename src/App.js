import './App.css';
/* imports for router */
import {
  BrowserRouter,
  Switch,
  Route,
} from 'react-router-dom'
import Login from './components/Login/Login.js'
import NotFound from './components/NotFound/NotFound';
import Translation from './components/Translation/Translation';
import Profile from './components/Profile/Profile';


function App() {
  // Keep track of wether user is logged in or not
  let loggedIn = false
  if (localStorage.getItem('currentUser') !== null) {
      loggedIn = true
  }

  return (
    /* Wrap application in order to use router  */
    <BrowserRouter>
        <div className="App bg-info .bg-indigo-xl">

          <div className="container mt-2 mb-2">
            <h1 className="col-sm">Sign Language Translator</h1>
            <img src={ loggedIn ? `assets/Logo.png` : 'assets/Logo-Hello.png' } width="150" alt="Logo" className="col-sm" />          
          </div>
          
          {/* Allows us to navigate between routes */}
          <Switch>
            <Route path="/" exact component={ Login } />
            <Route path="/translation" component={ Translation } />
            <Route path="/Profile" component={ Profile } />
            <Route path="*" component={ NotFound } />

          </Switch>
        </div>
    </BrowserRouter>
    
  );
}

export default App;
