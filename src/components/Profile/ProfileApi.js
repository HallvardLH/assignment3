// Function returns the last 10 translations of given user
export async function fetchTranslations(username) {
    let userData = {}
    // Get user data from database
    const response = await fetch(`http://localhost:8000/users?username=${username}`)
    if (response.ok){
        const data = await response.json()
        userData = data[0]
    }
    else{
        console.error(response.status);
    }

    // Return last 10 elements of array
    return userData.translations.slice(-10)

}

// Function that deletes all translations of given user in database
export async function deleteTranslations(username) {
    let userData = {}
    
    // PATCH empty array to translations in database
    const response = await fetch(`http://localhost:8000/users?username=${username}`, {
        method: 'PATCH',
        headers: {
            'Content-Type': 'application/json'
          },
        body: JSON.stringify({
            translations: []
        })
    })
    if (response.ok){
        const data = await response.json()
        userData = data[0]
    }
    else{
        console.error(response.status);
    }
    console.log(userData.translations);
    return userData.translations
}