import { useState, useEffect } from "react"
import { Redirect, Link } from "react-router-dom"
import AppContainer from "../../hoc/AppContainer"
import { useHistory } from "react-router"
import { fetchTranslations, deleteTranslations } from "./ProfileApi"

// Define component as a function. This represents the translation page
const Profile = () => {

    const history = useHistory()

    // Letters to be used for finding appropriate images
    const [sentences, setSentences] = useState([])

    // Array to store all translations
    const [letterArrays, setLetterArrays] = useState([])

    // Keep track of wether user is logged in or not
    let loggedIn = false
    if (localStorage.getItem('currentUser') !== null) {
        loggedIn = true
    }

    useEffect(() => {
        displayTranslations( )
    })

    // Log out
    const onLogoutClick = (event) =>{
        localStorage.clear()
        console.log("Log out");
        history.push("/")
    }

    // Clear data
    function onClearDataClick(event){
        console.log("Clear data");
        deleteTranslations(localStorage.getItem('currentUser'))
    }

    // Display translations
    async function displayTranslations(event){
        // Fetch translations
        const translations = await fetchTranslations(localStorage.getItem('currentUser'))

        // Update local state
        setSentences(translations) 

        const newArray = []

        for (let i = 0; i < translations.length; i++) {
            console.log(translations[i]);
            // Remove whitespace, then split string into letters and put in array
            newArray.push(translations[i].split(" ").join("").split(""))
        }
        // Update local state
        setLetterArrays(newArray)
    }

    return(
        <>
        {!loggedIn && <Redirect to="/"/>}
        <AppContainer>

            <Link to={"/translation"}>Go to translation page</Link>
                
            {/* Profile */}
            <div className="mb-3">
                <p>Welcome to your profile {localStorage.getItem('currentUser')}!</p>
            </div>


            <button onClick={ onLogoutClick } type="submit" className="btn btn-primary btn-lg mb-3">Log out</button>
            <button onClick={ onClearDataClick } type="submit" className="btn btn-primary btn-lg mb-3">Clear data</button>

            <div className="container mb-3 border bg-white rounded">
                    {/* Show images based on input letters*/}
                    { letterArrays.map((letter, index) => (
                        <div>
                            <p key={letter}>{ sentences[index] }</p>

                            {letterArrays[index].map((letter, index) => (
                                <img 
                                key={ index } 
                                src={ `assets/${letter}.png` }
                                alt={`${letter}`}/>
                            ))}            
                            
                        </div>
                    )) }
                </div>

        </AppContainer>
        </>
    )
}

export default Profile