import { useState } from "react"
import AppContainer from "../../hoc/AppContainer"
import { login } from "./LoginApi"
import { useHistory } from "react-router"
import { Redirect } from 'react-router-dom'

// Define component as a function. This represents the login page and form
const Login = () => {
    // For changing routes
    const history = useHistory()
    
    // Our useState receives one argument and that is the default state
    // We declare locale state username
    const [username, setUsername] = useState({
        username: '', // Default username is empty string
    })


    // Keep track of wether user is logged in or not
    let loggedIn = false
    if (localStorage.getItem('currentUser') !== null) {
        loggedIn = true
    }

    // Update local state on input change in input field
    const onInputChange = event => {
        setUsername({
            // Whatever the state was before needs to be added back to 
            // the state with the spread operator
            ...username,
            // event.target.id is the input where the event comes from
            username: event.target.value
        })
    }

    // Sumbit event shall register user in database
    const onFormSubmit = (event) =>{
        event.preventDefault()
        // Login. Register user in db if not already existing
        loginToDatabase(username.username)
        console.log("submit login")
        // Set user as current user in local storage
        localStorage.setItem('currentUser', username.username)
        // Change to translation route
        history.push("/translation")   
    }

    async function loginToDatabase(){
        await login(username.username)
    }

    return(
        <>
        {/* If user is logged in we redirect to translation page */}
        { loggedIn && <Redirect to="/translation" />}
        <AppContainer>
            <form className="mt-3" onSubmit={ onFormSubmit }>
                <h1>Login please</h1>
                
                {/* Username */}
                <div className="mb-3">
                    <label htmlFor="username" className="form-label" />
                    <input 
                        id="username" 
                        type="text"
                        placeholder="Enter your username"
                        className="form-control" 
                        onInput={ onInputChange }/>
                </div>

                <button type="submit" className="btn btn-primary btn-lg mb-3">Login</button>
            </form>
        </AppContainer>
        </>
    )
}

export default Login