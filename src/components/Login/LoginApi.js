export async function login(username) {

    // Get user and check if user exists
    const existingUser = await getUser(username)
    console.log("Existing user: ", existingUser);

    // If user already exists, length is > 0. Then return out of function
    if (existingUser.length > 0) {
        return 
    }
    
    // If user is not already registered, POST to database
    const response = await fetch('http://localhost:8000/users', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
          },
        body: JSON.stringify({
            username,
            translations: []
        })
    })

    const data = await response.json()
    return data
}

// Get user from database and return json
async function getUser(username){
    const url = `http://localhost:8000/users?username=${username}`
    const response = await fetch(url)
    // Log error if there is an error
    if (!response.ok) {
        const { error = 'An unknown error occurred' } = await response.json()
        throw new Error(error)
    }
    const data = await response.json()
    return data
     
}