// Function adds what user wanted to translate to the database
export async function updateTranslations(username, text) {
    let userData = {}
    // Get user data from database
    const response = await fetch(`http://localhost:8000/users?username=${username}`)
    if (response.ok){
        const data = await response.json()
        userData = data[0]
    }
    else{
        console.error(response.status);
    }

    console.log("User retrieved:", userData);
    console.log("Translation:", text);

    // Add new text to translations array and update database with new data
    userData.translations.push(text)

    console.log(userData.translations);

    // PATCH updates value of translations of appropriate user in database
    await fetch(`http://localhost:8000/users/${userData.id}`, {
        method: 'PATCH',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            translations: userData.translations,
        })
    })
}