import { useState } from "react"
import { Redirect, Link } from "react-router-dom"
import AppContainer from "../../hoc/AppContainer"
import { updateTranslations } from "./TranslationApi"

// Define component as a function. This represents the translation page
const Translation = () => {

    // Letters to be used for finding appropriate images
    const [letters, setLetters] = useState([])

    // We declare locale state translation
    const [translation, setTranslation] = useState({
        translation: '', // Default translation is empty string
    })

    // Keep track of wether user is logged in or not
    let loggedIn = false
    if (localStorage.getItem('currentUser') !== null) {
        loggedIn = true
    }

    // Update local state on input change in input field
    const onInputChange = event => {
        setTranslation({
            ...translation,
            translation: event.target.value
        })
    }

    // Sumbit event shall find translation and output correct images.
    // User input shall be saved in database
    async function onFormSubmit(event){
        event.preventDefault()
        // Remove whitespace, then split string into letters and put in array
        const letterArray = translation.translation.split(" ").join("").split("")
        // Update local state
        setLetters(letterArray) 
        // Save input to database
        await updateTranslations(localStorage.getItem('currentUser'), translation.translation)
    }

    return(
        <>
        {!loggedIn && <Redirect to="/"/>}
        <AppContainer>
            <p>Welcome {localStorage.getItem('currentUser')}!</p>
            <Link to={"/profile"}>Go to your profile page</Link>
            <form className="mt-3" onSubmit={ onFormSubmit }>
                
                {/* Translation */}
                <div className="mb-3">
                    <label htmlFor="translation" className="form-label" />
                    <input 
                        id="translation" 
                        type="text" 
                        placeholder="Enter phrase to translate" 
                        className="form-control"
                        onChange={ onInputChange }/>
                </div>

                <button type="submit" className="btn btn-primary btn-lg mb-3">Translate</button>

                <div className="container mb-3 border bg-white rounded">
                    {/* Show images based on input letters*/}
                    { letters.map((letter, index) => (
                        <img 
                            key={ index } 
                            src={ `assets/${letter}.png` }
                            alt={`${letter}`}/>
                    )) }
                </div>
            </form>
        </AppContainer>
        </>
    )
}

export default Translation