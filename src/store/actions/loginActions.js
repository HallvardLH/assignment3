export const ACTION_LOGIN_ATTEMPTING = '[login] ATTEMPT'
export const ACTION_LOGIN_SUCCESS = '[login] SUCCESS'
export const ACTION_LOGIN_ERROR = '[login] ERROR'

// These functions return an object. The objects have one or two props.
// One of the is type, the other the payload which is the
// data that is being transmitted
export const loginAttemptAction = username => ({
    type: ACTION_LOGIN_ATTEMPTING,
    payload: username
})

export const loginSuccessAction = username => ({
    type: ACTION_LOGIN_SUCCESS,
    payload: username
})

export const loginErrorAction = username => ({
    type: ACTION_LOGIN_ERROR,
    payload: username
})