import { LoginAPI } from "../../components/Login/LoginAPI"
import { ACTION_LOGIN_ATTEMPTING, ACTION_LOGIN_SUCCESS, loginErrorAction, loginSuccessAction } from "../actions/loginActions"

export const loginMiddleware = ({ dispatch }) => next => action => {

    next(action)

    // If we are attempting to login
    if (action.type === ACTION_LOGIN_ATTEMPTING) {
        // MAKE AN HTTP REQUEST to try and login
       LoginAPI.login(action.payload)
        // If successful login
        .then(profile => {
            dispatch(loginSuccessAction(profile))
        })
        // If there is an error
        .catch(error => {
            dispatch(loginErrorAction(error.message))
        }) 
    }

    if (action.type === ACTION_LOGIN_SUCCESS) {
        // WOW! Nice work, I'm now logged in! -> Do something now
    }
}